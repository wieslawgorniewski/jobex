FactoryBot.define do
  sequence :email do |n|
    "tester#{n}@tester.com"
  end

  factory :user do
    role 'jobseeker'
    password 'secret'
    email { generate(:email) }
  end
end
