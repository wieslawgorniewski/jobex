FactoryBot.define do
  sequence :title do |n|
    "Offer no #{n}"
  end

  sequence :employer_name do |n|
    "Employer Name no #{n}"
  end

  factory :offer do
    title { generate(:title) }
    description { generate(:title) }
    employer_name { generate(:employer_name) }
    user { create(:user, role: 'employer') }
  end
end
