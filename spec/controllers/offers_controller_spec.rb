require 'rails_helper'
require 'json'

RSpec.describe OffersController, type: :controller do
  before :each do
    @jobseeker = create(:user)
    @employer = create(:user, role: 'employer')
    @employer_2 = create(:user, role: 'employer')
    @post_data = {
      offer: {
        title: 'Blah Blah',
        description: 'Nice',
        employer_name: 'Pixie Jobex'
      }
    }
  end

  context 'employer' do
    before :each do
      sign_in @employer
    end

    describe 'POST #create' do
      it 'creates offer' do
        post :create, params: @post_data
        expect(response).to have_http_status(201)
        expect(Offer.where(user: @employer).count).to eq(1)
      end

      it 'does not create offer with invalid data' do
        @post_data[:offer].delete(:title)

        post :create, params: @post_data
        expect(response).to have_http_status(422)
        expect(response.body).to eq(
          {
            success: false,
            errors: ["Title can't be blank"]
          }.to_json
        )
        expect(Offer.all.count).to eq(0)
      end

      describe 'GET #index' do
        it 'gets all offers for signed employer' do
          create(:offer, user: @employer)
          create(:offer, user: @employer_2)
          create(:offer, user: @employer)

          get :index
          expect(response).to have_http_status(200)
          data_json = JSON.parse(response.body)
          expect(data_json.length).to eq(2)
          expect(data_json[0]['user_id']).to eq(@employer.id)
          expect(data_json[1]['user_id']).to eq(@employer.id)
        end

        it 'includes applicants' do
          offer = create(:offer, user: @employer)
          create(:application, user: @jobseeker, offer: offer)
          create(:application, offer: offer)

          get :index
          expect(response).to have_http_status(200)
          data_json = JSON.parse(response.body)
          expect(data_json.length).to eq(1)
          expect(data_json[0]['applications'].length).to eq(2)
          expect(data_json[0]['applications'][0]['jobseeker_email']).to eq(@jobseeker.email)
          expect(data_json[0]['applications'][1]['jobseeker_email']).not_to eq(@jobseeker.email)
        end
      end
    end
  end

  context 'jobseeker' do
    before :each do
      sign_in @jobseeker
    end

    describe 'POST #create' do
      it 'does not allow to create offer' do
        post :create, params: @post_data
        expect(response).to have_http_status(422)
        expect(Offer.all.count).to eq(0)
        expect(response.body).to eq(
          {
            success: false,
            errors: ['User is not Employer.']
          }.to_json
        )
      end

      describe 'GET #index' do
        it 'gets all offers from all employers' do
          create(:offer, user: @employer)
          create(:offer, user: @employer_2)
          create(:offer, user: @employer)

          get :index
          expect(response).to have_http_status(200)
          data_json = JSON.parse(response.body)
          expect(data_json.length).to eq(3)
          expect(data_json[0]['user_id']).to eq(@employer.id)
          expect(data_json[1]['user_id']).to eq(@employer_2.id)
          expect(data_json[2]['user_id']).to eq(@employer.id)
        end

        it ' does not include applicants' do
          offer = create(:offer, user: @employer)
          create(:application, user: @jobseeker, offer: offer)
          create(:application, offer: offer)

          get :index
          expect(response).to have_http_status(200)
          data_json = JSON.parse(response.body)
          expect(data_json.length).to eq(1)
          expect(data_json[0]['applications']).to be(nil)
        end
      end
    end
  end
end
