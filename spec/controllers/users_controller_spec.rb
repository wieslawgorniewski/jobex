require 'rails_helper'

RSpec.describe UsersController, type: :controller do

  before :each do
    @user = create(:user)
  end

  describe 'GET #current' do
    it 'responds with http 200 for signed user' do
      sign_in @user
      get :current, format: :json
      expect(response).to have_http_status(200)
      expect(response.body).to eq UserSerializer.new(@user).to_json
    end

    it 'responds with http 401 for unsigned user' do
      get :current, format: :json
      expect(response).to have_http_status(401)
    end
  end
end
