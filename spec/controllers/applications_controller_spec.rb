require 'rails_helper'

RSpec.describe ApplicationsController, type: :controller do

  before :each do
    @jobseeker = create(:user)
    @jobseeker_2 = create(:user)
    @employer = create(:user, role: 'employer')
    @offer = create(:offer, user: @employer)
    @post_data = {
      application: {
        user_id: @jobseeker.id,
        offer_id: @offer.id
      }
    }
    sign_in @jobseeker
  end

  describe 'POST #create' do
    it 'creates application for signed user' do
      post :create, params: @post_data
      expect(response).to have_http_status(201)
      expect(Application.where(user: @jobseeker, offer: @offer).count).to eq 1
    end

    it 'does not allow to apply for unsigned user' do
      sign_out @jobseeker
      post :create, params: @post_data, format: :json
      expect(response).to have_http_status(401)
      expect(Application.all.count).to eq 0
    end

    it 'does not allow to apply twice' do
      create(:application, user: @jobseeker, offer: @offer)
      post :create, params: @post_data
      expect(response).to have_http_status(422)
      expect(response.body).to eq(
        {
          success: false,
          errors: [
            'Offer has already been taken'
          ]
        }.to_json
      )
      expect(Application.where(user: @jobseeker, offer: @offer).count).to eq 1
    end

    it 'does allow to apply to 2 different offers' do
      @offer_2 = create(:offer, user: @employer)
      create(:application, user: @jobseeker, offer: @offer_2)
      post :create, params: @post_data
      expect(response).to have_http_status(201)
      expect(Application.where(user: @jobseeker).count).to eq 2
    end

    it 'does not allow to apply when user is logged as employer' do
      sign_in @employer
      post :create, params: {
        application: {
          user_id: @employer.id,
          offer_id: @offer.id
        }
      }
      expect(response).to have_http_status(422)
      expect(response.body).to eq(
        {
          success: false,
          errors: [
            'User is not Jobseeker.'
          ]
        }.to_json
      )
      expect(Application.where(user: @jobseeker).count).to eq 0
    end

    it 'does not allow to apply as other user' do
      sign_in @jobseeker_2
      post :create, params: @post_data
      expect(response).to have_http_status(403)
      expect(response.body).to eq(
        {
          success: false,
          errors: [
            "You can't apply for someone else!"
          ]
        }.to_json
      )
      expect(Application.all.count).to eq 0
    end
  end
end
