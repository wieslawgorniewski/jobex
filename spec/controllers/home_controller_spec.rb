require 'rails_helper'

RSpec.describe HomeController, type: :controller do
  render_views

  describe 'GET #index' do
    it 'responds with http 200 and renders template' do
      get :index, format: "html"
      expect(response).to have_http_status(200)
      expect(response).to render_template('layouts/application')
    end
  end
end
