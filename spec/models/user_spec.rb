require 'rails_helper'

RSpec.describe User, type: :model do

  describe 'set_default_role' do
    it 'sets default role to jobseeker if not given' do
      u = User.new(email: 'aaa@bbb.com', password: 'secret')
      u.save
      u.reload
      expect(u.role).to eq('jobseeker')
    end

    it 'does not set role to jobseeker if given' do
      u = User.new(email: 'aaa@bbb.com', password: 'secret', role: 'employer')
      u.save
      u.reload
      expect(u.role).to eq('employer')
    end
  end

  describe 'applied_to_offers' do
    it 'returns list of offers that jobseeker applied to' do
      user = create(:user)
      offer_1 = create(:offer)
      offer_2 = create(:offer)
      application_1 = create(:application, user: user, offer: offer_1)
      application_2 = create(:application, user: user, offer: offer_2)
      application_3 = create(:application)  # Not related
      expect(user.applied_to_offers).to eq([offer_1.id, offer_2.id])
    end
  end
end
