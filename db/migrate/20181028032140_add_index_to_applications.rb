class AddIndexToApplications < ActiveRecord::Migration[5.2]
  def change
    add_index :applications, [:offer_id, :user_id], :unique => true
  end
end
