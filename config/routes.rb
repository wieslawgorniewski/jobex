Rails.application.routes.draw do
  devise_for :users
  root 'home#index'
  scope :api do
    get '/users/current/', to: 'users#current' # Not very RESTful - Wieslaw
    resources :offers
    resources :applications
  end
  match "*path", to: 'home#index', via: :get
end
