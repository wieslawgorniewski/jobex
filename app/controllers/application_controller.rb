class ApplicationController < ActionController::Base
  before_action :configure_permitted_parameters, if: :devise_controller?
  skip_before_action :verify_authenticity_token

  def after_sign_in_path_for(resource)
    root_path
  end

  def errors(resource)
    errors = resource.errors.full_messages
    render json: {
      success: false,
      errors: errors,
    }, status: :unprocessable_entity
  end

  protected
  def configure_permitted_parameters
    devise_parameter_sanitizer.permit(:sign_up, keys: [:role])
  end
end
