class ApplicationsController < ApplicationController
  before_action :authenticate_user!
  before_action :validate_payload_user_id_correct!, only: [:create]

  def create
    application = Application.new(application_params)
    if application.save
      return render json: {
        success: true,
        resource: application
      }, status: :created
    end
    return errors(application)
  end

  private
  def application_params
    params.require(:application).permit(:offer_id, :user_id)
  end

  def validate_payload_user_id_correct!
    user_id = application_params.fetch(:user_id, 0).to_i
    unless user_id == current_user.id
      render json: {
        success: false,
        errors: [
          "You can't apply for someone else!"
        ],
      }, status: :forbidden
    end
  end
end
