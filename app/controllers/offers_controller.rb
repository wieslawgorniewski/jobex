class OffersController < ApplicationController
  before_action :authenticate_user!
  before_action :get_offers

  def index
    render json: @offers, status: :ok, each_serializer: serializer_for_model
  end

  def create
    offer = Offer.new(offer_params)
    offer.user = current_user
    if offer.save
      return render json: {
        success: true,
        resource: offer
      }, status: :created
    end
    return errors(offer)
  end

  private
  def offer_params
    params.require(:offer).permit(:title, :employer_name, :description)
  end

  def get_offers
    @offers = []
    if current_user.jobseeker?
      @offers = Offer.all
    else
      @offers = Offer.where(user: current_user)
    end
  end

  def serializer_for_model
    if current_user.employer?
      return OfferEmployerSerializer
    end
    OfferSerializer
  end
end
