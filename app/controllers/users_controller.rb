class UsersController < ApplicationController
  before_action :authenticate_user!

  def current
    render json: current_user, status: :ok, each_serializer: UserSerializer
  end
end
