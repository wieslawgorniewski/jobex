class Application < ActiveRecord::Base
  belongs_to :offer
  belongs_to :user
  validates :offer_id, uniqueness: { scope: :user_id }
  validate :user_is_jobseeker

  def jobseeker_email
    self.user.email
  end

  def user_is_jobseeker
    unless self.user.jobseeker?
      errors.add('user', 'is not Jobseeker.')
    end
  end
end
