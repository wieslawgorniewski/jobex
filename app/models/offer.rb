class Offer < ActiveRecord::Base
  belongs_to :user
  has_many :applications
  validates :employer_name, presence: true
  validates :title, presence: true
  validates :user, presence: true
  validate :user_is_employer

  def user_is_employer
    unless self.user.employer?
      errors.add('user', 'is not Employer.')
    end
  end
end
