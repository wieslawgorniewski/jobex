class User < ActiveRecord::Base
  has_many :offers
  has_many :applications
  enum role: [:jobseeker, :employer]
  after_initialize :set_default_role, :if => :new_record?

  devise :database_authenticatable, :registerable,
         :recoverable, :rememberable, :validatable

  def set_default_role
    self.role ||= :jobseeker
  end

  def applied_to_offers
    Application.where(
      user: self
    ).pluck(:offer_id)
  end
end
