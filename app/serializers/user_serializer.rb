class UserSerializer < ActiveModel::Serializer
  attributes :id,
             :email,
             :role,
             :applied_to_offers
end
