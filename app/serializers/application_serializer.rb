class ApplicationSerializer < ActiveModel::Serializer
  attributes :id,
             :user_id,
             :offer_id,
             :jobseeker_email
end
