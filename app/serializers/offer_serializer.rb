class OfferSerializer < ActiveModel::Serializer
  attributes :id,
             :title,
             :employer_name,
             :user_id,
             :description
end
