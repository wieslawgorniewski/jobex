import Route from 'route-parser';

const ROUTES = {
  'offers': new Route('/'),
  'offer_new': new Route('/offers/new/'),
  'login': new Route('/login/'),
}

export default ROUTES;
