import { Route, withRouter } from 'react-router-dom';
import React from 'react';

import ROUTES from './routes';
import Login from './login';
import Offers from './offers';
import OfferNew from './offer_new';


class Body extends React.Component {
  constructor() {
    super();

    this.getUser = this.getUser.bind(this);
    this.logout = this.logout.bind(this);
    this.redirectToLogin = this.redirectToLogin.bind(this);
    this.state = {
      currentUser: {}
    }
  }

  redirectToLogin() {
    if (window.location.pathname != '/login/') {
      window.location = '/login/';
    }
  }

  getUser () {
    fetch(
      '/api/users/current.json',
      {
        cache: "no-cache"
      }
    ).then((response) => {
      console.log(response.status);
        if (response.status < 400) {
          response.json().then((data) => {
            this.setState((state) => ({currentUser: data}))
          });
        } else {
          this.redirectToLogin();
        }
      }
    )
    .catch((err) => {
      this.redirectToLogin();
    });
  }

  logout() {
    fetch('/users/sign_out/', {method: 'DELETE'}).then((response) => {
        this.setState((state) => ({currentUser: {}}));
        redirectToLogin();
      }
    )
    .catch((err) => {
      this.setState((state) => ({currentUser: {}}));
      this.redirectToLogin();
    });
  }

  componentWillMount () {
    this.getUser();
  }

  render() {
    const RenderBody = () => {
      if (!this.state.currentUser.id) {
        return <div>
          <Route path={ROUTES.login.spec} exact render={(props) => (<Login currentUser={this.state.currentUser} />)}/>
        </div>
      }
      return <div>
        <Route path={ROUTES.offer_new.spec} exact render={(props) => (<OfferNew currentUser={this.state.currentUser} />)}/>
        <Route path={ROUTES.offers.spec} exact render={(props) => (<Offers currentUser={this.state.currentUser} />)}/>
      </div>
    }

    const LogoutButton = () => {
      if (!this.state.currentUser.id) {
        return ''
      }
      return <button className="uk-button uk-button-secondary uk-position-top-right" onClick={() => {this.logout()}}>
        Sign Out
      </button>
    }

    return (
      <div id="body">
        <LogoutButton />
        <RenderBody />
      </div>
    );
  }
}

const BodyWithRoute = withRouter(props => <Body {...props}/>);

export default BodyWithRoute;
