import React from 'react';
import OffersForJobSeekers from './offers_for_jobseekers'
import OffersForJobEmployers from './offers_for_employers'

class Offers extends React.Component {
  constructor() {
    super();
  }

  render() {
    const RenderOffers = () => {
      if (this.props.currentUser.role == 'jobseeker') {
        return <OffersForJobSeekers currentUser={this.props.currentUser} />
      }
      return <OffersForJobEmployers currentUser={this.props.currentUser} />
    }

    return (
      <div className="uk-margin-large-top">
        <RenderOffers />
      </div>
    )
  }
}

export default Offers;
