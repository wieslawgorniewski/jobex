import React from 'react';

class OffersForJobSeekers extends React.Component {
  constructor() {
    super();

    this.apply = this.apply.bind(this);
    this.state = {
      offers: [],
      appliedTo: []
    }
  }

  apply(offer_id, user_id) {
    fetch(
      '/api/applications/',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify({
          application: {
            offer_id: offer_id,
            user_id: user_id
          }
        })
      }
    ).then((response) => {
        if (response.status < 400) {
          let appliedTo = this.state.appliedTo;
          appliedTo.push(offer_id);
          this.setState((state) => ({appliedTo: appliedTo}))
        }
      }
    )
    .catch(function(err) {});
  }

  componentWillMount () {
    this.setState((state) => ({appliedTo: this.props.currentUser.applied_to_offers}))
    fetch('/api/offers/').then((response) => {
        if (response.status < 400) {
          response.json().then((data) => {
            this.setState((state) => ({offers: data}))
          });
        }
      }
    )
    .catch(function(err) {});
  }

  render() {
    const Offer = (data) => {
      let o = data.offer;
      let buttonDisabled = this.props.currentUser.applied_to_offers.indexOf(o.id) > -1;
      let buttonText = (buttonDisabled) ? 'You already applied' : 'Apply';

      return <div className="uk-card uk-card-default uk-padding-large uk-margin-small-bottom">
        <h2 className="uk-margin-small-bottom">
          Job Title: {o.title}
        </h2>
        <h4 className="uk-margin-small-top">
          Employer: {o.employer_name}
        </h4>
        <p>
          Description:
        </p>
        <p>
          {o.description || 'Not yet described.'}
        </p>
        <button className="uk-button uk-button-primary uk-position-bottom-right"
                onClick={
                  () => {
                    this.apply(
                      o.id,
                      this.props.currentUser.id
                    )
                  }
                }
                disabled={buttonDisabled}>
          {buttonText}
        </button>
      </div>
    }

    return (
      <div>
        <ul className="uk-list">
          {
            this.state.offers.map(
              (el, elIndex) => <Offer key={elIndex} offer={el} />
            )
          }
        </ul>
      </div>
    )
  }
}

export default OffersForJobSeekers;
