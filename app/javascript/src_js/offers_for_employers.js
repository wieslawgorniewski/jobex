import React from 'react';

class OffersForJobEmployers extends React.Component {
  constructor() {
    super();

    this.state = {
      offers: [],
    }
  }

  componentWillMount () {
    fetch('/api/offers/').then((response) => {
        if (response.status < 400) {
          response.json().then((data) => {
            this.setState((state) => ({offers: data}))
          });
        }
      }
    )
    .catch(function(err) {});
  }

  render() {
    const Alert = (data) => {
      let applications = data.applications || [];
      console.log(applications)
      if (applications.length > 0) {
        return <div className="uk-alert uk-alert-success">
          <h4>Great, you have these applicants (by email): </h4>
          <ul className="uk-list uk-list-bullet">
            {
              applications.map(
                (el, elIndex) => {
                  return <li key={elIndex}>{el.jobseeker_email}</li>
                }
              )
            }
          </ul>
        </div>
      }
      return '';
    }

    const Offer = (data) => {
      let o = data.offer;

      return <div className="uk-card uk-card-default uk-padding-large uk-margin-small-bottom">
        <h2 className="uk-margin-small-bottom">
          Job Title: {o.title}
        </h2>
        <h4 className="uk-margin-small-top">
          Employer: {o.employer_name}
        </h4>
        <p>
          Description:
        </p>
        <p>
          {o.description || 'Not yet described.'}
        </p>
        <Alert applications={o.applications} />
      </div>
    }

    return (
      <div>
        <button className="uk-button uk-button-primary" onClick={() => {window.location = '/offers/new/'}}>
          Add New Offer
        </button>
        <ul className="uk-list">
          {
            this.state.offers.map(
              (el, elIndex) => <Offer key={elIndex} offer={el} />
            )
          }
        </ul>
      </div>
    )
  }
}

export default OffersForJobEmployers;
