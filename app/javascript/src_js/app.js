import { Router } from 'react-router-dom';
import React from 'react';
import ReactDOM from 'react-dom';

import BodyWithRoute from './body'
import history from './history';

class App extends React.Component {
  render() {
    return (
      <Router history={history}>
        <BodyWithRoute />
      </Router>
    );
  }
}

const render = () => ReactDOM.render(
  <App />,
  document.getElementById('app')
);

render()
