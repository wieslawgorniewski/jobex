import React from 'react';

class OfferNew extends React.Component {
  constructor() {
    super();

    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleSubmit(event) {
    event.preventDefault();
    let formData = new FormData(event.target);
    let data = {};
    for (let [key, value]  of formData.entries()) {
        data[key] = value;
    }
    fetch(
      '/api/offers/',
      {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json',
          'Accept': 'application/json'
        },
        body: JSON.stringify(data)
      }
    ).then((response) => {
        if (response.status < 400) {
          window.location = '/';
        }
      }
    )
    .catch(function(err) {});
  }

  render() {
    return (
      <div className="uk-margin-large-top">
        <button className="uk-button uk-button-default uk-button-small" onClick={() => {window.location = '/'}}>
          To Offers
        </button>
        <h2>Add New Offer</h2>
        <form id="offerCreateForm" onSubmit={this.handleSubmit}>
            <div className="uk-margin">
                <input className="uk-input" name="title" type="text" placeholder="Name"></input>
            </div>
            <div className="uk-margin">
                <input className="uk-input" name="employer_name" type="text" placeholder="Employer Name"></input>
            </div>
            <div className="uk-margin">
                <textarea className="uk-textarea" name="description" rows="5" placeholder="Description"></textarea>
            </div>
            <button className="uk-button uk-button-primary">
              Create
            </button>
        </form>
      </div>
    )
  }
}

export default OfferNew;
