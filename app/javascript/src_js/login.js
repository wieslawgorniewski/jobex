import React from 'react';

class Login extends React.Component {
  constructor() {
    super();
  }

  render() {

    return (
      <div className="uk-margin-large-top">
        <div className="uk-grid uk-child-width-1-2@m uk-grid-small uk-grid-match ">
            <div>
                <div className="uk-card uk-card-secondary uk-card-body pointer" onClick={() => (window.location = '/users/sign_in/?role=jobseeker')}>
                    <h3 className="uk-card-title">For Jobseekers.</h3>
                    <p>
                      Login to see current Job Offers.
                    </p>
                </div>
            </div>
            <div>
                <div className="uk-card uk-card-primary uk-card-body pointer" onClick={() => (window.location = '/users/sign_in/?role=employer')}>
                    <h3 className="uk-card-title">For Employers.</h3>
                    <p>
                      Login to manage current Job Offers.
                    </p>
                </div>
            </div>
        </div>
      </div>
    )
  }
}

export default Login;
